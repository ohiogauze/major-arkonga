import Cards from "./cards";
import cardURLs from "./assets/cards/*.jpg";

function getURL (id: string) {
    return cardURLs[id] || cardURLs.unknown;
}

function restart (element: HTMLElement) {
    const name = element.style.animationName;
    element.style.animationName = "reset";
    element.style.animationPlayState = "paused";
    void element.offsetWidth;
    element.style.animationName = name;
    element.style.animationPlayState = "running";
}

const list = document.getElementById("tarot-list-cards");
const siteInfo = document.getElementById("site-info");
const tarotCard = (document.getElementById("tarot-info-card") as HTMLImageElement);
const tarotDescription = document.getElementById("tarot-description");
const tarotInfo = document.getElementById("tarot-info");
const tarotLink = (document.getElementById("tarot-link") as HTMLLinkElement);
const tarotTitle = document.getElementById("tarot-title");

Cards.forEach((card) => {
    const div = document.createElement("div");
    div.className = "tarot-list-cards-div";
    const img = document.createElement("img");
    img.src = getURL(card.id);
    div.appendChild(img);
    list.appendChild(div);

    img.onclick = () => {
        siteInfo.style.display = "none";
        tarotInfo.style.display = "flex";

        tarotTitle.innerText = card.name;
        tarotDescription.innerText = cardURLs[card.id] ? card.description : "TBA";
        tarotCard.src = img.src;

        tarotLink.style.display = card.redbubble ? "flex" : "none";
        tarotLink.href = card.redbubble;

        restart(tarotTitle);
        restart(tarotDescription);
        restart(tarotLink);
        restart(tarotCard);
    }
});