interface Card {
    id: string;
    description: string;
    name: string;
    redbubble?: string;
}

const Cards: Card[] = [
    {
        id: "the-fool",
        description: "A young Kong handstands on the edge of a cliff, without a care in the world, as he sets out on a new adventure. He is gazing upwards toward the sky (and the Universe) and is seemingly unaware that he is about to flip off a precipice into the unknown.  And at his feet is a small Banana Bird, representing loyalty and protection, that encourages him to charge forward and learn the lessons he came to learn. The mountains behind the Fool symbolise the challenges yet to come. They are forever present, but the Fool doesn’t care about them right now; he’s more focused on starting his expedition.",
        name: "The Fool",
        redbubble: "https://www.redbubble.com/people/muppetbutler/works/40066245-the-fool-dkc-tarot"
    },
    {
        id: "the-magician",
        description: "Description of The Magician",
        name: "The Magician"
    },
    {
        id: "the-high-priestess",
        description: "Description of The High Priestess",
        name: "The High Priestess"
    },
    {
        id: "the-empress",
        description: "Description of The Empress",
        name: "The Empress"
    },
    {
        id: "the-emperor",
        description: "Description of The Emperor",
        name: "The Emperor"
    },
    {
        id: "the-hierophant",
        description: "Description of The Hierophant",
        name: "The Hierophant"
    },
    {
        id: "the-lovers",
        description: "Description of The Lovers",
        name: "The Lovers"
    },
    {
        id: "strength",
        description: "Description of Strength",
        name: "Strength"
    },
    {
        id: "the-hermit",
        description: "Description of The Hermit",
        name: "The Hermit"
    },
    {
        id: "wheel-of-fortune",
        description: "Description of Wheel of Fortune",
        name: "Wheel of Fortune"
    },
    {
        id: "justice",
        description: "Description of Justice",
        name: "Justice"
    },
    {
        id: "the-hanged-man",
        description: "Description of The Hanged Man",
        name: "The Hanged Man"
    },
    {
        id: "death",
        description: "Description of Death",
        name: "Death"
    },
    {
        id: "temperance",
        description: "Description of Temperance",
        name: "Temperance"
    },
    {
        id: "the-devil",
        description: "Description of The Devil",
        name: "The Devil"
    },
    {
        id: "the-tower",
        description: "Description of The Tower",
        name: "The Tower"
    },
    {
        id: "the-star",
        description: "Description of The Star",
        name: "The Star"
    },
    {
        id: "the-moon",
        description: "Description of The Moon",
        name: "The Moon"
    },
    {
        id: "the-sun",
        description: "Description of The Sun",
        name: "The Sun"
    },
    {
        id: "judgement",
        description: "Description of Judgement",
        name: "Judgement"
    },
    {
        id: "the-world",
        description: "Description of The World",
        name: "The World"
    },
];

export default Cards;